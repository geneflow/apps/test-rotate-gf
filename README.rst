Test Rotate GeneFlow App
========================

Version: 0.1

This GeneFlow app performs a basic text rotation.

Inputs
------

1. input: Text file to rotate.

Parameters
----------

1. rotate: Number of characters to rotate. Default: 1.
 
2. output: Output text file.

